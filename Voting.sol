// SPDX-License-Identifier: MIT
pragma solidity 0.8.20;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/Context.sol";
import ".deps/npm/@openzeppelin/contracts/access/Ownable.sol";


contract Voting is Ownable {

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
    }

    struct Proposal {
        string description;
        uint voteCount;
    }

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    WorkflowStatus public currentStatus;

    mapping(address => Voter) public voters;
    mapping(address => address) public delegate;
    mapping(uint => uint) public proposalReports;
    mapping(uint => address) public proposalAuthors;

    Proposal[] public proposals;
    address[] public voterAddresses;

    uint public winningProposalId;

    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event Voted(address voter, uint proposalId);

    constructor(address initialOwner) Ownable(initialOwner) {
        currentStatus = WorkflowStatus.RegisteringVoters;
    }

    // Permet à l'owner d'enregistrer un votant
    function registerVoter(address _voterAddress) external onlyOwner {
        require(currentStatus == WorkflowStatus.RegisteringVoters, "L'enregistrement des votants est termine");
        require(!voters[_voterAddress].isRegistered, "L'electeur est deja enregistre");

        voters[_voterAddress].isRegistered = true;
        voterAddresses.push(_voterAddress); // ajouter l'adresse à la liste
        emit VoterRegistered(_voterAddress);
    }

    // Permet à l'owner de démarrer la session d'enregistrement des propositions
    function startProposalsRegistration() external onlyOwner {
        require(currentStatus == WorkflowStatus.RegisteringVoters,"Etat incorrect pour demarrer l'enregistrement des propositions");

        currentStatus = WorkflowStatus.ProposalsRegistrationStarted;
        emit WorkflowStatusChange(WorkflowStatus.RegisteringVoters, currentStatus);
    }

    // Permet de soumettre une proposition
    function submitProposal(string calldata _description) external {
        require(voters[msg.sender].isRegistered, "Seuls les votants enregistres peuvent soumettre des propositions");
        require(currentStatus == WorkflowStatus.ProposalsRegistrationStarted, "L'enregistrement des propositions n'est pas ouvert");

        proposals.push(Proposal({
            description: _description,
            voteCount: 0
        }));

        uint proposalId = proposals.length - 1;
        proposalAuthors[proposalId] = msg.sender;

        emit ProposalRegistered(proposalId);
    }

    // Permet à l'owner de terminer la session d'enregistrement des propositions
    function endProposalsRegistration() external onlyOwner {
        require(currentStatus == WorkflowStatus.ProposalsRegistrationStarted, "Etat incorrect pour terminer l'enregistrement des propositions");

        currentStatus = WorkflowStatus.ProposalsRegistrationEnded;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationStarted, currentStatus);
    }

    // Permet à l'owner de lancer la session de vote
    function startVotingSession() external onlyOwner {
        require(currentStatus == WorkflowStatus.ProposalsRegistrationEnded, "Etat incorrect pour demarrer la session de vote");

        currentStatus = WorkflowStatus.VotingSessionStarted;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationEnded, currentStatus);
    }

    // Permet aux votants présent dans la liste des addreses autorisées de voter pour une proposition et prend en compte si il y a une délagation de vote
    function vote(uint _proposalId) public {
        require(currentStatus == WorkflowStatus.VotingSessionStarted, "La session de vote n'est pas active");
        require(voters[msg.sender].isRegistered, "Vous n'etes pas un electeur enregistre");
        require(!voters[msg.sender].hasVoted, "Vous avez deja vote");
        require(_proposalId < proposals.length, "Proposition non valide");

        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = _proposalId;
        proposals[_proposalId].voteCount++;
        emit Voted(msg.sender, _proposalId);

        address voterAddress = delegate[msg.sender];
        while (voterAddress != address(0) && !voters[voterAddress].hasVoted) {
            voters[voterAddress].hasVoted = true;
            voters[voterAddress].votedProposalId = _proposalId;
            proposals[_proposalId].voteCount++;
            emit Voted(voterAddress, _proposalId);

            voterAddress = delegate[voterAddress];
        }
    }

    // Permet à l'owner de clôturer la session de vote
    function endVotingSession() external onlyOwner {
        require(currentStatus == WorkflowStatus.VotingSessionStarted, "Etat incorrect pour terminer la session de vote");

        currentStatus = WorkflowStatus.VotingSessionEnded;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionStarted, currentStatus);
    }

    // Permet à l'owner de comptabiliser les votes et déterminer le gagnant
    function tallyVotes() external onlyOwner {
        require(currentStatus == WorkflowStatus.VotingSessionEnded, "Etat incorrect pour comptabiliser les votes");

        uint winningVoteCount = 0;
        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > winningVoteCount) {
                winningVoteCount = proposals[i].voteCount;
                winningProposalId = i;
            }
        }

        currentStatus = WorkflowStatus.VotesTallied;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionEnded, currentStatus);
    }

    // Permet d'obtenir le détail de la proposition gagnante
    function getWinner() external view returns (string memory) {
        require(currentStatus == WorkflowStatus.VotesTallied, "Les votes n'ont pas encore ete comptabilises");
        return proposals[winningProposalId].description;
    }

    //Permet à l'owner de reset complétement le vote en remettant l'état à RegisteringVoters
    function resetVoting() public onlyOwner {
        require(currentStatus == WorkflowStatus.VotesTallied,"Le vote n'est pas encore termine");
        currentStatus = WorkflowStatus.RegisteringVoters;

        for (uint i = 0; i < voterAddresses.length; i++) {
            address voterAddress = voterAddresses[i];
            voters[voterAddress].hasVoted = false;
            voters[voterAddress].votedProposalId = 0;
            delegate[voterAddress] = address(0);
        }

        emit WorkflowStatusChange(WorkflowStatus.VotesTallied, WorkflowStatus.RegisteringVoters);
    }

    // Permet à un votant de déléguer son vote à un autre votant dans le cas où il ne serait pas présent pendant le vote par exemple
    function delegateVote(address to) public {
        require(currentStatus == WorkflowStatus.VotingSessionStarted, "La session de vote n'est pas active");
        require(voters[msg.sender].isRegistered, "L'emetteur doit etre un electeur enregistre");
        require(!voters[msg.sender].hasVoted, "L'emetteur a deja vote");
        require(voters[to].isRegistered, "Le destinataire de la delegation doit etre un electeur enregistre");
        delegate[msg.sender] = to;
    }

    // Permet à une personne qui a proposé une proposition de pouvoir la retirer pendant la session d'enregistrement des propositions
    function withdrawProposal(uint _proposalId) public {
        require(currentStatus == WorkflowStatus.ProposalsRegistrationStarted, "La session d'enregistrement n'est pas active");
        require(proposalAuthors[_proposalId] == msg.sender, "Seul l'auteur peut retirer sa proposition");
        delete proposals[_proposalId];
        delete proposalAuthors[_proposalId];
    }

    // Permet à un votant de signaler une proposition qui pourrait être incorrect, la proposition est supprimée si la moitié des votants la signale
    function reportProposal(uint _proposalId) public {
        require(currentStatus == WorkflowStatus.VotingSessionStarted || currentStatus == WorkflowStatus.ProposalsRegistrationStarted, "La session d'enregistrement ou de vote doit etre active");
        require(voters[msg.sender].isRegistered, "Seul un electeur enregistre peut signaler une proposition");
        proposalReports[_proposalId]++;
        if(proposalReports[_proposalId] > (address(this).balance / 2)) {
            delete proposals[_proposalId];
            delete proposalAuthors[_proposalId];
        }
    }
}
